#include "iss.h"
void ISS::setup() {}
void ISS::getNextPass()
{
    DEBUG_PRINTP(F("Updating ISS Information"));
    if (WiFi.status() == WL_CONNECTED)
    {
        String alt = config->get("home", "alt");
        String lon = config->get("home", "lon");
        String lat = config->get("home", "lat");
        String url = "http://api.open-notify.org/iss-pass.json?lat=" + lat + "&lon=" + lon + "&alt=" + alt + "&n=1";
        String payload = http.get(url.c_str()); //Specify request destination
        if (payload.length() > 0)
        {
            DynamicJsonBuffer jsonBuffer(200);
            JsonObject &data = jsonBuffer.parseObject(payload);
            if (data.success())
            {
                DEBUG_PRINT(SUCCESS, true);
                _iss->setRise(data["response"][0]["risetime"]);
                _iss->setDuration(data["response"][0]["duration"]);
                _iss->calculateFall();
                _stats->iss_requests++;
            }
            else
            {
                _stats->iss_errors++;
                DEBUG_PRINT(FAILED, true);
            }
        }
        else
        {
            _stats->iss_errors++;
            DEBUG_PRINT(FAILED, true);
        }
    }
}
void ISS::run()
{
    if (config->get("iss", "enabled"))
    {
        unsigned long timestamp = now() - config->get("home", "timezone").as<uint32_t>();
        if ((timeStatus() != timeNotSet && _iss->getRise() != 0) && (timestamp >= _iss->getRise()) && (timestamp < _iss->getFall()) && !passing)
        {
            effect->queue.effect = "rainbowCycle";
            effect->queue.delay = 5;
            passing = true;
            _stats->iss_passes++;
        }
        if ((timeStatus() != timeNotSet && _iss->getRise() != 0) && (timestamp >= _iss->getFall()) && passing)
        {
            passing = false;
            effect->resetStrip();
            getNextPass();
        }
    }
}
