#ifndef ISS_H
#define ISS_H
#include <globals.h>
#include <common.h>
#include <HttpClient.h>
class ISS
{
  public:
    void setup();
    void getNextPass();
    void run();

  private:
    HttpClient http;
    bool passing;
};
#endif // ISS_H
