#include "timer.h"

void Timer::setInterval(unsigned long _interval, CALLBACK f)
{
    interval = _interval;
    callback_ = f;
}

void Timer::run()
{
    unsigned long ms = millis();
    if (ms - last_ms > interval)
    {
        callback_();
        last_ms = ms;
    }
}
