#include "rf.h"
RF::RF()
{
    for (int i = 0; i < 5; i++)
    {
        _sensors->nodes[i] = new Sensor();
    }
}
void RF::setup()
{
    radio->begin();
    radio->setRetries(15, 15);
    radio->setPayloadSize(16);
    radio->openReadingPipe(0, pipes[0]);
    radio->setDataRate(RF24_250KBPS);
    radio->setPALevel(RF24_PA_MAX);
    radio->setChannel(120);
    radio->startListening();
}
void RF::run()
{
    if (radio->available())
    {
        radio->read(&payload, sizeof(payload));
        unsigned long ms = millis();
        uint8_t index = payload.uuid;
        unsigned long i = (ms - _sensors->nodes[index].last_activity) / 1000;
        _sensors->nodes[index].temperature = payload.temperature;
        _sensors->nodes[index].humidity = payload.humidity;
        _sensors->nodes[index].heat_index = payload.heat_index;
        _sensors->nodes[index].delay = (ms - _sensors->nodes[index].last_activity) / 1000;
        _sensors->nodes[index].last_activity = ms;
    }
}
