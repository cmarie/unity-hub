#ifndef RF_H
#define RF_H
#include "globals.h"
#include "common.h"
#include "nRF24L01.h"
#include "RF24.h"
class RF
{
  public:
    RF();
    void setup();
    void run();

  private:
    Packet payload;
    RF24 *radio = new RF24(2, 15); // D8 D10
    unsigned long last;
    const uint64_t pipes[1] = {0xF0F0F0F0CELL};
};
#endif // RF_H
