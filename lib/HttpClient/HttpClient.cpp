#include "HttpClient.h"
String HttpClient::get(const char *_url)
{
    String callback;
    if (WiFi.status() == WL_CONNECTED)
    {                     //Check WiFi connection status
        HTTPClient http;  //Declare an object of class HTTPClient
        http.begin(_url); //Specify request destination
        int httpCode = http.GET();
        if (httpCode == 200)
        {                                //Check the returning code
            callback = http.getString(); //Get the request response payload
        }
        http.end(); //Close connection
    }
    return callback;
}
