#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H
#include <Arduino.h>
#include <globals.h>
#include <ESP8266Wifi.h>
#include <ESP8266HTTPClient.h>
class HttpClient
{
public:
  String get(const char *_url);
};
#endif // HTTPCLIENT_H
