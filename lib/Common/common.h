#ifndef COMMON_H
#define COMMON_H

#include <Arduino.h>
#include <TimeLib.h>
#include <FS.h>
#include <effects.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
struct Config
{
    DynamicJsonBuffer buffer;
    JsonVariant data;
    bool reset = false;
    Config()
    {
        String json;
        if (SPIFFS.begin())
        {
            File f = SPIFFS.open("/config.json", "r");
            if (f)
            {
                while (f.available())
                {
                    json += char(f.read());
                }
                f.close();
            }
        }
        this->data = this->buffer.parseObject(json);
    }
    void setReset(bool _state)
    {
        reset = _state;
    }
    void write(String data)
    {
        if (SPIFFS.begin())
        {
            File f = SPIFFS.open("/config.json", "w");
            if (f)
            {
                f.println(data);
                f.close();
            }
        }
    }
    JsonVariant get(String k, String v)
    {
        return this->data[k][v];
    }
};
// Structs
struct Packet
{
    float uuid = 0;
    float temperature = 0;
    float humidity = 0;
    float heat_index = 0;
};
struct Sensor
{
    float temperature;
    float humidity;
    float heat_index;
    uint16_t delay = 0;
    unsigned long last_activity = 0;
};

struct Sensor_Data
{
    Sensor nodes[MAX_SENSOR_NODES];
};
struct ISS_Data
{
  private:
    unsigned long rise;
    unsigned long fall;
    uint16_t duration;

  public:
    void setRise(unsigned long _val)
    {
        this->rise = _val;
    }
    void setDuration(uint16_t _val)
    {
        this->duration = _val;
    }
    void calculateFall()
    {
        this->fall = this->getRise() + this->getDuration();
    }
    unsigned long getRise()
    {
        return this->rise;
    }
    unsigned long getFall()
    {
        return this->fall;
    }
    uint16_t getDuration()
    {
        return this->duration;
    }
};
/*
struct Weather_Alert
{
  private:
    const char * description;
    const char * type;
    unsigned long expires;

  public:
    void setDescription(const char *_description)
    {
        this->description = _description;
    }
    void setType(const char *_type)
    {
        this->type = _type;
    }
    void setExpires(unsigned long _expires)
    {
        this->expires = _expires;
    }
    const char *getType()
    {
        return this->type;
    }
    const char *getDescription()
    {
        return this->description;
    }
    unsigned long getExpires()
    {
        return this->expires;
    }
};
struct Weather_Data
{
  private:
    const char * condition;
    const char * icon;
    const char * location;
    double temp;
    double humidity;
    double wind_speed;
    double feels_like;
    uint8_t alert_count;

  public:
    Weather_Alert *alerts[MAX_WEATHER_ALERTS];
    void setCondition(const char *_val)
    {
        this->condition = _val;
    }
    void setIcon(const char *_val)
    {
        this->icon = _val;
    }
    void setLocation(const char *_val)
    {
        this->location = _val;
    }
    void setTemp(double _val)
    {
        this->temp = _val;
    }
    void setHumidity(double _val)
    {
        this->humidity = _val;
    }
    void setWindSpeed(double _val)
    {
        this->wind_speed = _val;
    }
    void setFeelsLike(double _val)
    {
        this->feels_like = _val;
    }
    void setAlertCount(uint8_t _val)
    {
        this->alert_count = _val;
    }
    uint8_t getAlertCount()
    {
        return this->alert_count;
    }
    const char *getCondition()
    {
        return this->condition;
    }
    const char *getIcon()
    {
        return this->icon;
    }
    const char *getLocation()
    {
        return this->location;
    }
    double getHumidity()
    {
        return this->humidity;
    }
    double getTemp()
    {
        return this->temp;
    }
    double getFeelsLike()
    {
        return this->feels_like;
    }
    double getWindSpeed()
    {
        return this->wind_speed;
    }
};
*/
struct Weather_Alert
{
  private:
    char description[30];
    char type[8];
    unsigned long expires;

  public:
    void setDescription(const char *_description)
    {
        strcpy(this->description, _description);
    }
    void setType(const char *_type)
    {
        strcpy(this->type, _type);
    }
    void setExpires(unsigned long _expires)
    {
        this->expires = _expires;
    }
    const char *getType()
    {
        return this->type;
    }
    const char *getDescription()
    {
        return this->description;
    }
    unsigned long getExpires()
    {
        return this->expires;
    }
};
struct Weather_Data
{
  private:
    char condition[30];
    char icon[15];
    char location[30];
    double temp;
    double humidity;
    double wind_speed;
    double feels_like;
    uint8_t alert_count;

  public:
    Weather_Alert *alerts[MAX_WEATHER_ALERTS];
    void setCondition(const char *_val)
    {
        strcpy(this->condition, _val);
    }
    void setIcon(const char *_val)
    {
        strcpy(this->icon, _val);
    }
    void setLocation(const char *_val)
    {
        strcpy(this->location, _val);
    }
    void setTemp(double _val)
    {
        this->temp = _val;
    }
    void setHumidity(double _val)
    {
        this->humidity = _val;
    }
    void setWindSpeed(double _val)
    {
        this->wind_speed = _val;
    }
    void setFeelsLike(double _val)
    {
        this->feels_like = _val;
    }
    void setAlertCount(uint8_t _val)
    {
        this->alert_count = _val;
    }
    uint8_t getAlertCount()
    {
        return this->alert_count;
    }
    char *getCondition()
    {
        return this->condition;
    }
    char *getIcon()
    {
        return this->icon;
    }
    char *getLocation()
    {
        return this->location;
    }
    double getHumidity()
    {
        return this->humidity;
    }
    double getTemp()
    {
        return this->temp;
    }
    double getFeelsLike()
    {
        return this->feels_like;
    }
    double getWindSpeed()
    {
        return this->wind_speed;
    }
};
struct Statistic_Data
{
    unsigned long weather_errors;
    unsigned long iss_errors;
    unsigned long time_errors;
    unsigned long reporting_errors;
    unsigned long json_errors;
    unsigned long iss_passes;
    unsigned long wifi_reconnects;
    unsigned long weather_requests;
    unsigned long iss_requests;
    unsigned long mqtt_requests;
    unsigned long mqtt_errors;
};

// Global Variables
extern Config *config;
extern ISS_Data *_iss;
extern Weather_Data *_weather;
extern Statistic_Data *_stats;
extern Sensor_Data *_sensors;
extern Effects *effect;

static String buildStats()
{
    StaticJsonBuffer<1200> jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();

    // Device Information
    JsonObject &device = root.createNestedObject("device");
    device["voltage"] = ESP.getVcc() / 1024.00f;
    device["free_heap"] = ESP.getFreeHeap();
    device["free_sketch_space"] = ESP.getFreeSketchSpace();
    device["uptime"] = millis() / 1000;
    device["time"] = now();
    device["build_date"] = __DATE__ " " __TIME__;

    // WiFi Information
    JsonObject &wifi = root.createNestedObject("wifi");
    wifi["ip"] = WiFi.localIP().toString();
    wifi["subnet"] = WiFi.subnetMask().toString();
    wifi["ssid"] = WiFi.SSID();
    wifi["bssid"] = WiFi.BSSIDstr();
    wifi["rssi"] = 2 * (WiFi.RSSI() + 100);
    wifi["channel"] = WiFi.channel();

    // Weather Information
    JsonObject &weather = root.createNestedObject("weather");
    weather["condition"] = _weather->getCondition();
    weather["icon"] = _weather->getIcon();
    weather["temp"] = _weather->getTemp();
    weather["humidity"] = _weather->getHumidity();
    weather["location"] = _weather->getLocation();
    weather["wind_speed"] = _weather->getWindSpeed();
    weather["feels_like"] = _weather->getFeelsLike();
    JsonArray &alerts = weather.createNestedArray("alerts");
    for (int i = 0; i < _weather->getAlertCount(); i++)
    {
        JsonObject &alert = alerts.createNestedObject();
        alert["description"] = _weather->alerts[i]->getDescription();
        alert["expires"] = _weather->alerts[i]->getExpires();
    }
    // Sensor Information
    JsonObject &sensors = root.createNestedObject("sensors");
    JsonArray &nodes = sensors.createNestedArray("nodes");
    for (int i = 0; i < MAX_SENSOR_NODES; i++)
    {
        if (_sensors->nodes[i].last_activity > 0)
        {
            JsonObject &sensor = nodes.createNestedObject();
            sensor["temperature"] = _sensors->nodes[i].temperature;
            sensor["humidity"] = _sensors->nodes[i].humidity;
            sensor["heat_index"] = _sensors->nodes[i].heat_index;
            sensor["delay"] = _sensors->nodes[i].delay;
        }
    }

    // ISS Information
    JsonObject &iss = root.createNestedObject("iss");
    iss["rise"] = _iss->getRise();
    iss["fall"] = _iss->getFall();
    iss["duration"] = _iss->getDuration();

    // Statistics
    JsonObject &stats = root.createNestedObject("statistics");

    // Error Statistics
    JsonObject &errors = stats.createNestedObject("errors");
    errors["weather"] = _stats->weather_errors;
    errors["iss"] = _stats->iss_errors;
    errors["time"] = _stats->time_errors;
    errors["json"] = _stats->json_errors;
    errors["mqtt"] = _stats->mqtt_errors;

    // Event Statistics
    JsonObject &events = stats.createNestedObject("events");
    events["iss_passes"] = _stats->iss_passes;
    events["wifi"] = _stats->wifi_reconnects;
    events["mqtt"] = _stats->mqtt_requests;
    events["weather"] = _stats->weather_requests;
    events["iss"] = _stats->iss_requests;

    String payload;
    root.printTo(payload);
    return payload;
}
#endif //COMMON_H
