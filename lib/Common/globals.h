#ifndef GLOBALS_H
#define GLOBALS_H
#define SECOND 1000
#define MINUTE 60 * SECOND
#ifdef DEBUG
#include <Arduino.h>

#define SUCCESS F("\x1b[32m\x1b[1m[OK]\033[0m\033[0m")
#define FAILED F("\x1b[31m[FAILED]\033[0m")
#define ERROR F("\x1b[31m[ERROR]\033[0m")
#define DISABLED F("\x1b[33m[DISABLED]\033[0m")
static String pad(String txt)
{
    int max_len = 50;
    int len = txt.length();
    for (int i = len; i < max_len; i++)
    {
        txt += " ";
    }
    return txt;
}

#define DEBUG_PRINTP(x) Serial.print(pad(x))
#define DEBUG_PRINT(x, y) y ? Serial.println(x) : Serial.print(x)
#define DEBUG_WRITE(x, y) Serial.write(x, y)
#else
#define DEBUG_PRINTP(x)
#define DEBUG_PRINT(x, y)
#define DEBUG_WRITE(x, y)

#endif
#endif // GLOBALS_H
