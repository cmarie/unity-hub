#include "ota.h"

OTA::OTA()
{
    ArduinoOTA.setPort(config->get("ota", "port").as<uint16_t>());
    ArduinoOTA.setHostname(config->get("network", "hostname").as<const char *>());
    ArduinoOTA.setPassword(config->get("ota", "password").as<const char *>());
}
void OTA::setup()
{
    ArduinoOTA.onStart([this]() {
        DEBUG_PRINT(F("Beginning OTA"), true);
        effect->resetStrip();
    });
    ArduinoOTA.onEnd([this]() {
        effect->solid(0, 255, 0);
        DEBUG_PRINT(F("\nOTA Complete"), true);
    });
    ArduinoOTA.onProgress([this](unsigned int _progress, unsigned int _total) {
        int prog = (_progress / (_total / 100));
        effect->progress(0, 0, 255, prog);
        DEBUG_PRINTP(F("Uploading... "));
        DEBUG_PRINT(F("\033[1;32m["), false);
        DEBUG_PRINT(prog, false);
        DEBUG_PRINT(F("%"), false);
        DEBUG_PRINT(F("]\033[0m\r"), false);
    });
    ArduinoOTA.onError([](ota_error_t _error) {
        if (_error == OTA_AUTH_ERROR)
        {
            DEBUG_PRINT(F("OTA Failed: Unable to authenticate"), true);
        }
        else if (_error == OTA_BEGIN_ERROR)
        {
            DEBUG_PRINT(F("OTA Failed: Unable to start"), true);
            //
        }
        else if (_error == OTA_CONNECT_ERROR)
        {
            DEBUG_PRINT(F("OTA Failed: Unable to connect"), true);
            //
        }
        else if (_error == OTA_RECEIVE_ERROR)
        {
            DEBUG_PRINT(F("OTA Failed: Unable to receive"), true);
            //
        }
        else if (_error == OTA_END_ERROR)
        {
            DEBUG_PRINT(F("OTA Failed: Unable to end"), true);
            //
        }
    });
    ArduinoOTA.begin();
}
void OTA::run()
{
    if (config->get("ota", "enabled"))
    {
        ArduinoOTA.handle();
    }
}
