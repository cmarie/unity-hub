#ifndef OTA_H_
#define OTA_H_

#include <globals.h>
#include <common.h>
#include <ArduinoOTA.h>
class OTA {
    public:
        OTA();
        void run();
        void setup();
};
#endif /* OTA_H_ */
