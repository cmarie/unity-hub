#include "device.h"

Device::Device() {}
void Device::setup()
{
    delay(5000);
#ifdef DEBUG
    Serial.begin(SERIAL_BAUD);
#endif
    DEBUG_PRINT("\n", true);
    wifi.setup();
    ntp.setup();
    DEBUG_PRINTP(F("Setting up OTA"));
    if (config->get("ota", "enabled"))
    {
        ota.setup();
        DEBUG_PRINT(SUCCESS, true);
    }
    else
    {
        DEBUG_PRINT(DISABLED, true);
    }
    DEBUG_PRINTP(F("Setting up ISS"));
    if (config->get("iss", "enabled"))
    {
        iss.setup();
        DEBUG_PRINT(SUCCESS, true);
    }
    else
    {
        DEBUG_PRINT(DISABLED, true);
    }
    server.setup();
    //rf.setup();
    effect->resetStrip();
    getData();

    DEBUG_PRINTP(F("Setting up MQTT"));
    if (config->get("mqtt", "enabled"))
    {
        mqtt.setup([this](char *_topic, byte *_payload, unsigned int _length) {
            mqttCallback(_topic, _payload, _length);
        });
        DEBUG_PRINT(SUCCESS, true);
    }
    else
    {
        DEBUG_PRINT(DISABLED, true);
    }
    if (config->get("iss", "enabled"))
    {
        iss.getNextPass();
    }
    tmr[0].setInterval(MINUTE, [this]() { // Every 1 minute
        sendData();
    });
    tmr[1].setInterval(15 * MINUTE, [this]() { // Every 15 minutes
        getData();
    });
}
void Device::mqttCallback(char *topic, byte *payload, unsigned int length)
{
    _stats->mqtt_requests++;
    DynamicJsonBuffer jsonBuffer(200);
    JsonObject &data = jsonBuffer.parseObject(payload);
    DEBUG_PRINTP(F("Parsing JSON Data"));
    if (data.success())
    {
        DEBUG_PRINT(SUCCESS, true);
        if (data.containsKey("effect"))
        {
            DEBUG_PRINTP(F("Processing Effect"));
            processEffect(data);
            DEBUG_PRINT(SUCCESS, true);
        }
    }
    else
    {
        _stats->json_errors++;
        DEBUG_PRINT(ERROR, true);
    }
}
void Device::processEffect(JsonObject &data)
{
    JsonObject &_effect = data["effect"];
    effect->resetStrip();
    effect->queue.effect = _effect["name"].as<char *>();
    if (_effect.containsKey("color"))
    {
        effect->queue.color = {_effect["color"]["r"], _effect["color"]["g"], _effect["color"]["b"]};
    }
    if (_effect.containsKey("delay"))
    {
        effect->queue.delay = _effect["delay"];
    }
    if (_effect.containsKey("balls"))
    {
        effect->queue.balls = _effect["balls"];
    }
    if (_effect.containsKey("decay"))
    {
        effect->queue.decay = _effect["decay"];
    }
    if (_effect.containsKey("size"))
    {
        effect->queue.size = _effect["size"];
    }
    if (_effect.containsKey("random"))
    {
        effect->queue.random = _effect["random"];
    }
    if (_effect.containsKey("fade"))
    {
        effect->queue.fade = _effect["fade"];
    }
}

void Device::sendData()
{
    if (config->get("mqtt", "publishing"))
    {
        if (mqtt.connected())
        {
            const char *topic = strdup(config->get("mqtt", "pub_topic"));
            mqtt.publish(topic, buildStats().c_str());
        }
    }
}

void Device::getData()
{
    ntp.getTime();
    if (config->get("wunderground", "enabled"))
    {
        weather.getAlerts();
        weather.getConditions();
        weather.processAlerts();
    }
}

void Device::run()
{
    wifi.run();
    ota.run();
    mqtt.run();
    iss.run();
    //rf.run();
    effect->run();
    server.run();
    tmr[0].run();
    tmr[1].run();
}
