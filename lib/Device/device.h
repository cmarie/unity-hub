#ifndef DEVICE_H_
#define DEVICE_H_

#include <globals.h>
#include <common.h>
#include <ntp.h>
#include <mqtt.h>
#include <wifi.h>
#include <ota.h>
#include <timer.h>
#include <weather.h>
#include <iss.h>
#include <HttpServer.h>
//#include <rf.h>
class Device
{
  public:
    Device();
    void setup();
    void run();

  private:
    Timer tmr[2]; // 0 = MQTT Publish 1 = Weather
    WIFI wifi;
    OTA ota;
    NTP ntp;
    ISS iss;
    MQTT mqtt;
    Weather weather;
    HttpServer server;
    //RF rf;
    void processEffect(JsonObject &data);
    void sendData();
    void getData();
    void mqttCallback(char *topic, byte *payload, unsigned int length);
};
#endif
