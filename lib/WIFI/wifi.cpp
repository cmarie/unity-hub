#include "wifi.h"
WIFI::WIFI()
{
    WiFi.mode(WIFI_STA);
    WiFi.setSleepMode(WIFI_NONE_SLEEP);
    WiFi.hostname(config->get("network", "hostname").as<String>());
}
WIFI::~WIFI() {}
void WIFI::setup()
{
    DEBUG_PRINTP(F("Connecting to Router"));
    int cnt = 0;
    while (WiFi.status() != WL_CONNECTED)
    {
        yield();
        delay(500);
        if (cnt++ >= 30)
        {
            DEBUG_PRINT(FAILED, true);
            WiFi.beginSmartConfig();
            DEBUG_PRINT(F("Waiting for SmartConfig"), false);
            while (1)
            {
                yield();
                delay(1000);
                if (WiFi.smartConfigDone())
                {
                    DEBUG_PRINT(SUCCESS, true);
                    break;
                }
                else if (cnt++ >= 60)
                {
                    DEBUG_PRINT(FAILED, true);
                    DEBUG_PRINT(F("Rebooting..."), true);
                    ESP.restart();
                }
            }
        }
    }
    DEBUG_PRINT(F("\033[1;32m["), false);
    DEBUG_PRINT(WiFi.localIP(), false);
    DEBUG_PRINT(F("]\033[0m"), true);
    DEBUG_PRINTP(F("Setting up mDNS"));
    if (MDNS.begin(config->get("network", "hostname")))
    {
        DEBUG_PRINT(SUCCESS, true);
    }
    else
    {
        DEBUG_PRINT(FAILED, true);
    }
}
void WIFI::run()
{
    if (WiFi.status() != WL_CONNECTED)
    {
        _stats->wifi_reconnects++;
        DEBUG_PRINT(F("Connection to router lost!"), true);
        DEBUG_PRINT(WiFi.SSID(), true);
        setup();
    }
}