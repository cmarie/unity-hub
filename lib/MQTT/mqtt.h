#ifndef MQTT_H
#define MQTT_H
#include <globals.h>
#include <common.h>
#include <PubSubClient.h>
typedef std::function<void(char *, byte *, unsigned int)> CALLBACK_;
class MQTT
{
  public:
    void setup(CALLBACK_ f);
    boolean connected();
    boolean publish(const char *topic, const char *payload);
    boolean publish(const char *topic, const char *payload, boolean retained);
    boolean publish(const char *topic, const uint8_t *payload, unsigned int plength);
    boolean publish(const char *topic, const uint8_t *payload, unsigned int plength, boolean retained);
    void run();

  private:
    WiFiClient espClient;
    PubSubClient client;
    unsigned long last_ms;
    void connect();
};
#endif // MQTT_H
