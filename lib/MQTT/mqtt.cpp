#include "mqtt.h"
void MQTT::setup(CALLBACK_ f)
{
    client.setClient(espClient);
    const char *hostname = strdup(config->get("mqtt", "host"));
    uint16_t port = config->get("mqtt", "port");
    client.setServer(hostname, port);
    client.setCallback(f);
    connected();
}
void MQTT::connect()
{
    if (config->get("mqtt", "enabled"))
    {
        DEBUG_PRINTP(F("Connecting to MQTT Server"));
        const char *hostname = strdup(config->get("network", "hostname"));
        const char *username = strdup(config->get("mqtt", "username"));
        const char *password = strdup(config->get("mqtt", "password"));
        if (client.connect(hostname, username, password))
        {
            DEBUG_PRINT(SUCCESS, true);
            DEBUG_PRINTP(F("Subscribing to Topic"));

            const char *topic = strdup(config->get("mqtt", "sub_topic"));
            if (!client.subscribe(topic))
            {
                DEBUG_PRINT(ERROR, true);
            }
            else
            {
                DEBUG_PRINT(SUCCESS, true);
            }
        }
        else
        {
            DEBUG_PRINT(FAILED, true);
        }
    }
}
boolean MQTT::connected()
{
    return client.connected();
}
boolean MQTT::publish(const char *topic, const char *payload)
{
    bool callback = false;
    if (connected())
    {
        DEBUG_PRINTP(F("Publishing Data"));
        callback = client.publish(topic, (const uint8_t *)payload, strlen(payload), false);
        if (!callback)
        {
            _stats->mqtt_errors++;
            DEBUG_PRINT(ERROR, true);
        }
        else
        {
            _stats->mqtt_requests++;
            DEBUG_PRINT(SUCCESS, true);
        }
    }
    return callback;
}

boolean MQTT::publish(const char *topic, const char *payload, boolean retained)
{
    DEBUG_PRINT(F("Sending: "), false);
    DEBUG_PRINT(payload, false);
    bool callback = client.publish(topic, (const uint8_t *)payload, strlen(payload), retained);
    if (!callback)
    {
        DEBUG_PRINT(ERROR, true);
    }
    else
    {
        DEBUG_PRINT(SUCCESS, true);
    }
    return callback;
}

boolean MQTT::publish(const char *topic, const uint8_t *payload, unsigned int plength)
{
    DEBUG_PRINT(F("Sending: "), false);
    DEBUG_WRITE(payload, plength);
    DEBUG_PRINT("\n", false);
    return client.publish(topic, payload, plength);
}

boolean MQTT::publish(const char *topic, const uint8_t *payload, unsigned int plength, boolean retained)
{
    DEBUG_PRINT(F("Sending: "), false);
    DEBUG_WRITE(payload, plength);
    DEBUG_PRINT("\n", false);
    return client.publish(topic, payload, plength, retained);
}
void MQTT::run()
{
    if (config->get("mqtt", "enabled"))
    {
        if (!client.connected())
        {
            connect();
        }
        client.loop();
    }
}
