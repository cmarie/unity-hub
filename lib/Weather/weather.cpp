#include "weather.h"
void Weather::getConditions()
{
    String key = config->get("wunderground", "key");
    String lon = config->get("home", "lon");
    String lat = config->get("home", "lat");
    String url = "http://api.wunderground.com/api/" + key + "/conditions/q/" + lat + "," + lon + ".json";
    DEBUG_PRINTP(F("Updating Weather Conditions"));
    String payload = http.get(url.c_str());
    _stats->weather_requests++;
    if (payload.length() > 0)
    {
        DynamicJsonBuffer jsonBuffer(3000);
        JsonObject &data = jsonBuffer.parseObject(payload);
        if (data.success())
        {
            DEBUG_PRINT(SUCCESS, true);
            _weather->setCondition(data["current_observation"]["weather"]);
            _weather->setLocation(data["current_observation"]["display_location"]["full"]);
            _weather->setIcon(data["current_observation"]["icon"]);
            _weather->setTemp(data["current_observation"]["temp_f"]);
            _weather->setWindSpeed(data["current_observation"]["wind_mph"]);
            _weather->setFeelsLike(data["current_observation"]["feelslike_f"]);
            _weather->setHumidity(data["current_observation"]["relative_humidity"]);
        }
        else
        {
            _stats->weather_errors++;
            DEBUG_PRINT(FAILED, true);
        }
    }
    else
    {
        _stats->weather_errors++;
        DEBUG_PRINT(FAILED, true);
    }
}
void Weather::getAlerts()
{
    _weather->setAlertCount(0);
    String key = config->get("wunderground", "key").as<String>();
    String lon = config->get("home", "lon").as<String>();
    String lat = config->get("home", "lat").as<String>();
    String url = "http://api.wunderground.com/api/" + key + "/alerts/q/" + lat + "," + lon + ".json";
    DEBUG_PRINTP(F("Updating Weather Alerts"));
    String payload = http.get(url.c_str());
    _stats->weather_requests++;

    if (payload.length() > 0)
    {
        DynamicJsonBuffer jsonBuffer(3000);
        JsonObject &data = jsonBuffer.parseObject(payload);
        if (data.success())
        {
            DEBUG_PRINT(SUCCESS, true);
            JsonArray &alerts = data["alerts"];
            _weather->setAlertCount(alerts.size());
            for (int i = 0; i < _weather->getAlertCount(); i++)
            {
                const char *description = alerts[i]["description"].as<const char *>();
                _weather->alerts[i] = new Weather_Alert();
                _weather->alerts[i]->setDescription(description);
                _weather->alerts[i]->setExpires(alerts[i]["expires_epoch"].as<unsigned long>() + config->get("home", "timezone").as<uint32_t>());
                if (strstr(description, "Warning"))
                {
                    _weather->alerts[i]->setType("warning");
                }
                else if (strstr(description, "Watch"))
                {
                    _weather->alerts[i]->setType("watch");
                }
                else if (strstr(description, "Special"))
                {
                    _weather->alerts[i]->setType("special");
                }
                else if (strstr(description, "Advisory") || strstr(description, "Statement"))
                {
                    _weather->alerts[i]->setType("advisory");
                }
            }
        }
        else
        {
            DEBUG_PRINT(FAILED, true);
            _stats->weather_errors++;
        }
    }
    else
    {
        _stats->weather_errors++;
        DEBUG_PRINT(FAILED, true);
    }
}
void Weather::processAlerts()
{
    if (strstr(_weather->getIcon(), "rain"))
    {
        effect->queue.effect = "sparkle";
        effect->queue.color.r = 0;
        effect->queue.color.g = 255;
        effect->queue.color.b = 0;
        effect->queue.fade = true;
        effect->queue.random = false;
        effect->queue.delay = 25;
    }
    else if (strstr(_weather->getIcon(), "snow"))
    {
        effect->queue.effect = "sparkle";
        effect->queue.color.r = 0;
        effect->queue.color.g = 0;
        effect->queue.color.b = 255;
        effect->queue.fade = true;
        effect->queue.random = false;
        effect->queue.delay = 50;
    }
    else if (strstr(_weather->getIcon(), "sleet"))
    {
        effect->queue.effect = "sparkle";
        effect->queue.color.r = 255;
        effect->queue.color.g = 255;
        effect->queue.color.b = 255;
        effect->queue.fade = true;
        effect->queue.random = false;
        effect->queue.delay = 25;
    }
    else if (_weather->getAlertCount() > 0)
    {
        effect->queue.effect = "weatherAlert";
        effect->queue.size = _weather->getAlertCount();
        for (int i = 0; i < _weather->getAlertCount(); i++)
        {
            effect->queue.alerts[i] = _weather->alerts[i]->getType();
        }
    }
    else if (strstr(effect->queue.effect, "sparkle"))
    {
        effect->resetStrip();
    }
}
