#ifndef WEATHER_H
#define WEATHER_H
#include <globals.h>
#include <common.h>
#include <HttpClient.h>
#include <ArduinoJson.h>
class Weather
{
public:
  void getConditions();
  void getAlerts();
  void processAlerts();

private:
  HttpClient http;
};
#endif // WEATHER_H
