#include "ntp.h"

void NTP::setup()
{
    udp.begin(config->get("ntp", "port").as<short int>());
}
void NTP::getTime()
{
    DEBUG_PRINTP(F("Updating RTC"));
    WiFi.hostByName(config->get("ntp", "server").as<char *>(), timeServerIP);

    sendNTPpacket(timeServerIP);
    bool set = false;
    unsigned long last_ms = millis();
    while (!set)
    {
        unsigned long ms = millis();
        yield();
        int cb = udp.parsePacket();
        if (cb)
        {
            udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
            unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
            unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
            unsigned long secsSince1900 = highWord << 16 | lowWord;
            const unsigned long seventyYears = 2208988800UL;
            setTime(secsSince1900 - seventyYears + config->get("home", "timezone").as<uint32_t>());
            set = true;
        }
        if (ms - last_ms > (10 * SECOND))
        {
            break;
        }
    }
    if (!set)
    {
        DEBUG_PRINT(FAILED, true);
    }
    else
    {
        DEBUG_PRINT(SUCCESS, true);
    }
}
unsigned long NTP::sendNTPpacket(IPAddress &address)
{
    // set all bytes in the buffer to 0
    memset(packetBuffer, 0, NTP_PACKET_SIZE);
    packetBuffer[0] = 0b11100011; // LI, Version, Mode
    packetBuffer[1] = 0;          // Stratum, or type of clock
    packetBuffer[2] = 6;          // Polling Interval
    packetBuffer[3] = 0xEC;       // Peer Clock Precision
    packetBuffer[12] = 49;
    packetBuffer[13] = 0x4E;
    packetBuffer[14] = 49;
    packetBuffer[15] = 52;
    udp.beginPacket(address, 123); //NTP requests are to port 123
    udp.write(packetBuffer, NTP_PACKET_SIZE);
    udp.endPacket();
}
