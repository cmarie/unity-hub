#ifndef NTP_H
#define NTP_H
#include <globals.h>
#include <common.h>
#include <WiFiUdp.h>
class NTP
{
  public:
    void setup();
    void getTime();

  private:
    WiFiUDP udp;
    IPAddress timeServerIP; // time.nist.gov NTP server address
    byte packetBuffer[48];
    unsigned long sendNTPpacket(IPAddress &address);
};
#endif // NTP_H
