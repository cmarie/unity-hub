#include "HttpServer.h"
HttpServer::HttpServer() {}
HttpServer::~HttpServer()
{
    delete server;
}
void HttpServer::setup()
{

    server = new ESP8266WebServer(config->get("http", "port").as<uint16_t>());
    DEBUG_PRINTP(F("Setting up Filesystem"));
    if (!SPIFFS.begin())
    {
        DEBUG_PRINT(FAILED, true);
    }
    else
    {
        DEBUG_PRINT(SUCCESS, true);
    }

    // HTML Files
    server->serveStatic("/", SPIFFS, "/index.html");

    // Serve all files
    Dir dir = SPIFFS.openDir("/");
    while (dir.next())
    {
        String filename = dir.fileName();
        server->serveStatic(filename.c_str(), SPIFFS, filename.c_str());
    }

    // XHR Files
    server->on("/data.json", HTTP_GET, [this]() {
        server->sendHeader("Access-Control-Allow-Origin", "*");
        server->send(200, "application/json", String(buildStats()));
    });

    // Saves config
    server->on("/save", HTTP_POST, [this]() {
        server->sendHeader("Access-Control-Allow-Origin", "*");
        config->write(server->arg("plain"));
        server->send(200, "application/json", "{\"code\": 200}");
        config->setReset(true);
    });

    // Effect Processor
    server->on("/trigger", HTTP_POST, [this]() {
        server->sendHeader("Access-Control-Allow-Origin", "*");
        DynamicJsonBuffer buffer;
        JsonObject &data = buffer.parseObject(server->arg("plain"));
        effect->resetStrip();
        effect->queue.effect = data["effect"].as<char *>();
        if (data.containsKey("color"))
        {
            effect->queue.color = {data["color"]["r"].as<uint8_t>(), data["color"]["g"].as<uint8_t>(), data["color"]["b"].as<uint8_t>()};
        }
        if (data.containsKey("delay"))
        {
            effect->queue.delay = data["delay"].as<uint16_t>();
        }
        if (data.containsKey("balls"))
        {
            effect->queue.balls = data["balls"].as<uint8_t>();
        }
        if (data.containsKey("decay"))
        {
            effect->queue.decay = data["decay"].as<uint8_t>();
        }
        if (data.containsKey("size"))
        {
            effect->queue.size = data["size"].as<uint8_t>();
        }
        if (data.containsKey("random"))
        {
            effect->queue.random = data["random"].as<bool>();
        }
        if (data.containsKey("fade"))
        {
            effect->queue.fade = data["fade"].as<bool>();
        }
        server->send(200, "application/json", "{\"code\": 200}");
    });

    // Soft Restarts Device
    server->on("/restart", [this]() {
        server->sendHeader("Access-Control-Allow-Origin", "*");
        server->send(200, "application/json", "{\"code\": 200}");
        config->setReset(true);

    });
    // SSDP Files
    server->on("/description.xml", HTTP_GET, [this]() {
        SSDP.schema(server->client());
    });
    DEBUG_PRINTP(F("Setting up HTTP Server"));
    server->begin();
    DEBUG_PRINT(SUCCESS, true);
    DEBUG_PRINTP(F("Setting up SSDP"));
    if (config->get("ssdp", "enabled"))
    {
        SSDP.setSchemaURL("description.xml");
        SSDP.setHTTPPort(config->get("http", "port").as<uint16_t>());
        SSDP.setDeviceType(config->get("ssdp", "device_type").as<char *>());
        SSDP.setName(config->get("ssdp", "name").as<char *>());
        SSDP.setSerialNumber(ESP.getChipId());
        SSDP.setURL("/index.html");
        SSDP.setModelName(config->get("ssdp", "model_name").as<char *>());
        SSDP.setModelNumber(config->get("ssdp", "model_number").as<char *>());
        SSDP.setModelURL(config->get("ssdp", "model_url").as<char *>());
        SSDP.setManufacturer(config->get("ssdp", "manufacturer").as<char *>());
        SSDP.setManufacturerURL(config->get("ssdp", "manufacturer_url").as<char *>());
        if (!SSDP.begin())
        {
            DEBUG_PRINT(FAILED, true);
        }
        else
        {
            DEBUG_PRINT(SUCCESS, true);
        }
    }
    else
    {
        DEBUG_PRINT(DISABLED, true);
    }
}
void HttpServer::run()
{
    server->handleClient();
}
