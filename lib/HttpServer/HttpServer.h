#ifndef HTTPSERVER_H
#define HTTPSERVER_H
#include <globals.h>
#include <common.h>
#include <ESP8266WebServer.h>
#include <ESP8266SSDP.h>
#include <FS.h>

class HttpServer
{
  public:
    HttpServer();
    ~HttpServer();
    void setup();
    void run();

  private:
    String deviceJson();
    ESP8266WebServer *server;
};

#endif // HTTPSERVER_H
