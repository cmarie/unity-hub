var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var htmlmin = require('gulp-htmlmin');

gulp.task('scripts', function () {
    return gulp.src([
        './libs/nite-overlay.js',
        './js/site.js',
        './js/iss.js',
        './js/settings.js',
        './js/effects.js'
    ])
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../data/js', { overwrite: true }))
        .pipe(gulp.dest('./js', { overwrite: true }))
});
gulp.task('root', function () {
    return gulp.src([
        './*.html',
        './config.json'
    ])
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('../data'))
});
gulp.task('pages', function () {
    return gulp.src([
        './pages/*.html',
    ])
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('../data/pages'))
});
gulp.task('css', function () {
    return gulp.src([
        './css/style.css'
    ])
        .pipe(gulp.dest('../data/css'))
});

gulp.task('default', gulp.series('scripts', 'root', 'pages', 'css'))
