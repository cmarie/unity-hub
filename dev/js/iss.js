var ISS = {
    map: null,
    issMarker: null,
    homeMarker: null,
    circle: null,
    geocoder: null,
    init: function () {
        ISS.setupMap();
        ISS.getSchedule();
    },
    setupMap: function () {
        ISS.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            styles: Site.mapStyle,
            disableDefaultUI: true,
            backgroundColor: 'none'
        });
        ISS.homeMarker = new google.maps.Marker({
            map: ISS.map,
            optimized: false,
            icon: new google.maps.MarkerImage('https://i.imgur.com/M1iuOU9.png', new google.maps.Size(53, 52), new google.maps.Point(0, 0), new google.maps.Point(53 / 2, 52 / 2)),
            position: {
                lat: parseFloat(Site.config.home.lat),
                lng: parseFloat(Site.config.home.lon)
            }
        });
        ISS.issMarker = new google.maps.Marker({
            map: ISS.map,
            optimized: false,
            icon: new google.maps.MarkerImage('https://i.imgur.com/LzfosaW.png', new google.maps.Size(78, 77), new google.maps.Point(0, 0), new google.maps.Point(78 / 2, 77 / 2))
        });
        ISS.circle = new google.maps.Circle({
            strokeWeight: 0,
            fillColor: '#323745',
            fillOpacity: 0.35,
            map: ISS.map
        });
        ISS.geocoder = new google.maps.Geocoder()
        nite.init(ISS.map);
        setInterval(function () {
            nite.refresh()
        }, 1000)
        this.updatePosition();
    },
    updatePosition: function () {
        var time = new Date();
        var timestamp = time.getTime() / 1000;
        $.getJSON('https://api.wheretheiss.at/v1/satellites/25544/positions?timestamps=' + timestamp + '&units=miles', function (results) {
            $.each(results, function (i, data) {
                var center = {
                    lat: parseFloat(data.latitude),
                    lng: parseFloat(data.longitude)
                };
                var bounds = new google.maps.LatLngBounds();
                var A = 6375;
                var C = A + data.altitude;
                var radius = Math.sqrt((C * C) - (A * A));
                ISS.issMarker.setPosition(center);
                ISS.circle.setRadius(radius * 1000);
                ISS.circle.setCenter(center);
                bounds.extend(ISS.issMarker.getPosition());
                bounds.extend(ISS.homeMarker.getPosition());
                ISS.map.fitBounds(bounds);


                ISS.geocoder.geocode({ location: center }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0].address_components[2]) {
                            $('#iss-location').text(results[0].formatted_address);
                        }
                    }
                });
                $('#iss-velocity').text(Number(data.velocity.toFixed()).toLocaleString("en-US"));
                $('#iss-elevation').text(Number(data.altitude.toFixed()).toLocaleString("en-US"));
                setTimeout(function () {
                    ISS.updatePosition()
                }, 10000)
            })
        })
    },
    getSchedule: function () {
        $.getJSON('http://api.open-notify.org/iss-pass.json?callback=?', {
            lon: Site.config.home.lon,
            lat: Site.config.home.lat,
            alt: Site.config.home.alt
        }, function (data) {
            $.each(data.response, function (k, v) {
                var riseTime = new Date(0);
                var fallTime = new Date(0);
                riseTime.setUTCSeconds(v.risetime);
                fallTime.setUTCSeconds(v.risetime + v.duration);
                var options = {
                    weekday: "long",
                    year: "numeric",
                    month: "short",
                    day: "numeric",
                    hour: "2-digit",
                    minute: "2-digit"
                };
                var tr = $('<tr/>');
                var rise = $('<td/>').text(riseTime.toLocaleTimeString("en-us", options));
                var fall = $('<td/>').text(fallTime.toLocaleTimeString("en-us", options));
                var duration = $('<td/>').text(Site.fancyTimeFormat(v.duration, false));
                $('#iss-schedule').append(tr.append(rise).append(fall).append(duration));
            });
        })
    }
}