var Settings = {
    data: null,
    map: null,
    marker: null,
    geocoder: null,
    elevator: null,
    init: function () {
        var data = Site.config;
        Settings.setupForm();
        $('#geocode').on("click", function (e) {
            e.preventDefault();
            Settings.lookup($("#home-lookup").val())
        })
        Settings.data = data;
        $('#home-lon').val(data.home.lon)
        $('#home-lat').val(data.home.lat)
        $('#home-alt').val(data.home.alt)
        $('#home-timezone').val(data.home.timezone)
        if (data.mqtt.enabled) {
            $('#mqtt-enabled').attr('checked', data.mqtt.enabled ? 'checked' : '')
        }
        if (data.mqtt.publishing) {
            $('#mqtt-publishing').attr('checked', data.mqtt.publishing ? 'checked' : '')
        }
        $('#mqtt-pub_topic').val(data.mqtt.pub_topic)
        $('#mqtt-sub_topic').val(data.mqtt.sub_topic)
        $('#mqtt-host').val(data.mqtt.host)
        $('#mqtt-port').val(data.mqtt.port)
        $('#mqtt-username').val(data.mqtt.username)
        $('#mqtt-password').val(data.mqtt.password)
        if (data.iss.enabled) {
            $('#iss-enabled').attr('checked', data.iss.enabled ? 'checked' : '')
        }
        if (data.ota.enabled) {
            $('#ota-enabled').attr('checked', data.ota.enabled ? 'checked' : '')
        }
        $('#ota-port').val(data.ota.port)
        $('#ota-password').val(data.ota.password)
        $('#ntp-server').val(data.ntp.server)
        $('#ntp-port').val(data.ntp.port)
        $('#google-key').val(data.google.key)
        $('#wunderground-key').val(data.wunderground.key)
        $('#network-hostname').val(data.network.hostname)
        $('#http-port').val(data.http.port)
        if (data.ssdp.enabled) {
            $('#ssdp-enabled').attr('checked', data.ssdp.enabled ? 'checked' : '')
        }
        $('#ssdp-device_type').val(data.ssdp.device_type)
        $('#ssdp-name').val(data.ssdp.name)
        $('#ssdp-model_name').val(data.ssdp.model_name)
        $('#ssdp-model_number').val(data.ssdp.model_number)
        $('#ssdp-model_url').val(data.ssdp.model_url)
        $('#ssdp-manufacturer').val(data.ssdp.manufacturer)
        $('#ssdp-manufacturer_url').val(data.ssdp.manufacturer_url)
        if (data.wunderground.enabled) {
            $('#wunderground-enabled').attr('checked', data.wunderground.enabled ? 'checked' : '')
        }
        Settings.setupMap()
    },
    setupForm: function () {
        var form = $('form');
        form.on("submit", function (e) {
            e.preventDefault();
            Site.showLoader();
            setTimeout(function () {
                window.location.reload(true);
            }, 10 * 1000);
            $.ajax({
                url: '/save',
                data: JSON.stringify({
                    "home": {
                        "lon": parseFloat($('#home-lon').val()),
                        "lat": parseFloat($('#home-lat').val()),
                        "alt": parseInt($('#home-alt').val()),
                        "timezone": parseInt($('#home-timezone').val())
                    },
                    "ntp": {
                        "server": $('#ntp-server').val(),
                        "port": parseInt($('#ntp-port').val())
                    },
                    "network": {
                        "hostname": $('#network-hostname').val()
                    },
                    "mqtt": {
                        "enabled": $('#mqtt-enabled').is(':checked'),
                        "publishing": $('#mqtt-publishing').is(':checked'),
                        "pub_topic": $('#mqtt-pub_topic').val(),
                        "sub_topic": $('#mqtt-sub_topic').val(),
                        "host": $('#mqtt-host').val(),
                        "port": parseInt($('#mqtt-port').val()),
                        "username": $('#mqtt-username').val(),
                        "password": $('#mqtt-password').val()
                    },
                    "ota": {
                        "enabled": $('#ota-enabled').is(':checked'),
                        "password": $('#ota-password').val(),
                        "port": parseInt($('#ota-port').val())
                    },
                    "google": {
                        "key": $('#google-key').val()
                    },
                    "iss": {
                        "enabled": $('#iss-enabled').is(':checked'),
                    },
                    "wunderground": {
                        "enabled": $('#wunderground-enabled').is(':checked'),
                        "key": $('#wunderground-key').val()
                    },
                    "http": {
                        "port": parseInt($('#http-port').val())
                    },
                    "ssdp": {
                        "enabled": $('#ssdp-enabled').is(':checked'),
                        "schema_url": $('#ssdp-schema_url').val(),
                        "device_type": $('#ssdp-device_type').val(),
                        "name": $('#ssdp-name').val(),
                        "model_name": $('#ssdp-model_name').val(),
                        "model_number": parseInt($('#ssdp-model_number').val()),
                        "model_url": $('#ssdp-model_url').val(),
                        "manufacturer": $('#ssdp-manufacturer').val(),
                        "manufacturer_url": $('#ssdp-manufacturer_url').val()
                    }
                }),
                contentType: "application/json",
                type: 'POST',
                dataType: 'json'
            })
        })
    },
    lookup: function (location) {
        Settings.geocoder.geocode({
            'address': location
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var loc = results[0].geometry.location;
                Settings.map.setCenter(results[0].geometry.location);
                Settings.marker.setPosition(loc);
                Settings.elevator.getElevationForLocations({
                    'locations': [loc]
                }, function (results, status) {
                    $.getJSON('https://maps.googleapis.com/maps/api/timezone/json', {
                        location: results[0].location.lat() + ',' + results[0].location.lng(),
                        key: Settings.data.google.key,
                        timestamp: Math.round((new Date()).getTime() / 1000)
                    }, function (response) {
                        $('#home-timezone').val(response.rawOffset)
                    })
                    $('#home-lat').val(results[0].location.lat().toFixed(7));
                    $('#home-lon').val(results[0].location.lng().toFixed(7));
                    $('#home-alt').val(results[0].elevation.toFixed(0))
                })
            } else {
                alert("Could not find location: " + location)
            }
        })
    },
    setupMap: function () {
        var location = Settings.data.home.lat + "," + Settings.data.home.lon;
        var myOptions = {
            zoom: 3,
            styles: Site.mapStyle,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        Settings.map = new google.maps.Map(document.getElementById("map"), myOptions);
        Settings.marker = new google.maps.Marker({
            map: Settings.map,
            optimized: false,
            icon: new google.maps.MarkerImage('https://i.imgur.com/M1iuOU9.png', new google.maps.Size(53, 52), new google.maps.Point(0, 0), new google.maps.Point(53 / 2, 52 / 2))
        });
        Settings.geocoder = new google.maps.Geocoder();
        Settings.elevator = new google.maps.ElevationService;
        Settings.map.addListener('click', function (event) {
            var location = event.latLng;
            Settings.marker.setPosition(location);
            Settings.elevator.getElevationForLocations({
                'locations': [location]
            }, function (results, status) {
                $('#home-lat').val(results[0].location.lat().toFixed(7));
                $('#home-lon').val(results[0].location.lng().toFixed(7));
                $('#home-alt').val(results[0].elevation.toFixed(0))
            })
        });
        nite.init(Settings.map);
        setInterval(function () {
            nite.refresh()
        }, 1000)
        Settings.lookup(location)
    }
}