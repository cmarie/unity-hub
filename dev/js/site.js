var Site = {
    container: $('#page'),
    page: 'dashboard',
    mapStyle: [
        {
            "featureType": "all",
            "elementType": "all",
            "stylers": [{ "visibility": "off" }]
        }, {
            "featureType": "administrative.country",
            "elementType": "geometry.stroke",
            "stylers": [{ "visibility": "on" }, { "color": "#363b4a" }, { "lightness": "-30" }]
        }, {
            "featureType": "administrative.country",
            "elementType": "labels",
            "stylers": [{ "visibility": "simplified" }, { "color": "#50b87f" }, { "lightness": "15" }]
        }, {
            "featureType": "administrative.province",
            "elementType": "geometry.stroke",
            "stylers": [{ "visibility": "on" }, { "color": "#363b4a" }, { "lightness": "-30" }]
        }, {
            "featureType": "administrative.locality",
            "elementType": "labels",
            "stylers": [{ "visibility": "simplified" }, { "color": "#363b4a" }, { "lightness": "30" }]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{ "color": "#363b4a" }, { "visibility": "simplified" }, { "lightness": "-40" }]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [{ "visibility": "on" }, { "color": "#2e323e" }]
        }, {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [{ "visibility": "on" }, { "color": "#4d5765" }]
        }, {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{ "visibility": "on" }, { "color": "#363b4a" }, { "lightness": "-30" }]
        }
    ],
    animations: { entrance: 'fadeIn', mouseover: 'rubberBand' },
    icons: {
        "overcast": "wi-day-sunny-overcast",
        "clear": "wi-day-sunny",
        "mostlycloudy": "wi-day-cloudy",
        "mostlysunny": "wi-day-sunny",
        "partlycloudy": "wi-day-cloudy",
        "partlysunny": "wi-day-sunny",
        "cloudy": "wi-day-cloudy",
        "hazy": "wi-haze",
        "flurries": "wi-snow-wind",
        "chanceflurries": "wi-snow-wind",
        "chancerain": "wi-rain",
        "chancesleat": "wi-sleet",
        "chancesnow": "wi-show",
        "rain": "wi-showers",
        "sleat": "wi-sleat",
        "sunny": "wi-sunny",
        "snow": "wi-snow",
        "tstorms": "wi-thunderstrom",
        "unknown": "wi-sunny"
    },
    date: new Date(),
    months: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
    config: {},
    init: function () {
        $.ajaxSetup({ cache: false });
        Site.getConfig();
    },
    getConfig: function () {
        $.getJSON('/config.json', function (data) {
            Site.config = data;
            $.getScript('https://maps.googleapis.com/maps/api/js?v=3&key=' + Site.config.google.key, function () {
                Site.setupNavigation();
                Site.loadPage("Dashboard");
            })
        });
    },
    setupElements: function () {
        $.getJSON('/data.json', function (data) {
            $.each(data, function (a, b) {
                a = a.replace('_', '-');
                $.each(b, function (c, d) {
                    c = c.replace('_', '-');
                    if (typeof d == 'object') {
                        $.each(d, function (e, f) {
                            if (typeof e != 'number') {
                                e = e.replace('_', '-');
                                $('#' + a + '-' + c + '-' + e).text(f).parent().attr('data-value', f);
                            }
                        });
                    } else {
                        $('#' + a + '-' + c).text(d).parent().attr('data-value', d);
                    }
                });
            });
            $('.month').text(Site.months[Site.date.getMonth()]);
            var day = Site.date.getDate();
            $('.day').text(day < 10 ? '0' + day : day);
            $('#weather-temp').text(parseFloat($('#weather-temp').text()).toFixed())
            $('#weather-feels-like').text(parseFloat($('#weather-feels-like').text()).toFixed())
            $('#w-icon').addClass('wi ' + Site.icons[data.weather.icon])
            $("input[type=checkbox]").bootstrapSwitch();
            Site.removeLoader();
            Site.setupProgressBars();
            $('a:not([target])', Site.container).on('click', function (e) {
                var el = $(this);
                if (el.data('url')) {
                    e.preventDefault();
                    var url = el.data('url');
                    var title = el.data('title');
                    Site.page = url;
                    Site.loadPage(title);
                }
            })
        })
    },
    loadPage: function (_title) {
        Site.container.removeClass(Site.animations.entrance).load('/pages/' + Site.page + '.html', function () {
            Site.setupElements();
            document.title = 'Unity - ' + _title;
            Site.container.addClass(Site.animations.entrance);
        });
    },
    setupProgressBars: function () {
        $('.progress-bar').each(function () {
            var el = $(this);
            var child = $('span', el);
            var value = el.data('value');
            if (child.attr('id') == 'wifi-rssi') {
                child.append('%');
                el.width(value + '%');
            }
            if (child.attr('id') == 'device-free-heap') {
                var percent = ((49072 - value) / 49072) * 100;
                el.width(percent + '%');
                child.text(percent.toFixed())
                child.append('%');
            }
            if (child.attr('id') == 'device-voltage') {
                var percent = value / 3.3 * 100;
                el.width(percent + '%');
                child.text(value.toFixed(2)).append('v');
            }
        });
    },
    setupNavigation: function () {
        var sidebar = $('#sidebar');
        $('a', sidebar).each(function () {
            var el = $(this);
            $('.icon', el).addClass('animated');
            if (Site.page == el.data('page')) {
                el.parent().addClass('active');
            }
        }).on('click', function (e) {
            e.preventDefault();
            var el = $(this);
            var page = el.data('page');
            if (page == 'restart') {
                $.getJSON('/restart');
                Site.showLoader();
                setTimeout(function () {
                    window.location.reload(true);
                }, 10 * 1000);
            } else {
                $('li.active', sidebar).removeClass('active');
                Site.page = el.data('page');
                el.parent().addClass('active');
                Site.loadPage(el.data('title'));
            }
        }).on('mouseover', function () {
            var el = $(this);
            $('.icon', el).addClass(Site.animations.mouseover)
        }).on('mouseleave', function () {
            var el = $(this);
            $('.icon*', el).removeClass(Site.animations.mouseover)
        })
    },
    removeLoader: function () {
        $('#loading').hide();
    },
    showLoader: function () {
        $('#loading').show();
    },
    fancyTimeFormat: function (time, a) {
        var hrs = ~~(time / 3600);
        var mins = ~~((time % 3600) / 60);
        var secs = time % 60;
        var ret = "";
        if (hrs > 0) {
            ret += "" + hrs + "h " + (mins < 10 ? "0" : "")
        }
        ret += "" + mins + (a ? " minutes " : "m ") + (secs < 10 ? "0" : "");
        ret += "" + secs + (a ? " seconds" : "s");
        return ret
    }
}
Site.init();