var Effects = {
    hex_to_RGB: function (hex) {
        var m = hex.match(/^#?([\da-f]{2})([\da-f]{2})([\da-f]{2})$/i);
        return {
            r: parseInt(m[1], 16),
            g: parseInt(m[2], 16),
            b: parseInt(m[3], 16)
        }
    },
    isNumber: function (n) {
        return !isNaN(parseFloat(n)) && isFinite(n)
    },
    init: function () {
        $('form').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            var elements = form.serializeArray();
            var data = {};
            for (var i = 0; i < elements.length; i++) {
                var el = elements[i];
                if (el.name == "color") {
                    data[el.name] = Effects.hex_to_RGB(el.value)
                } else if (el.value == "on" || el.value == "off") {
                    data[el.name] = el.value == "on"
                } else {
                    data[el.name] = Effects.isNumber(el.value) ? parseInt(el.value) : el.value
                }
            }
            $.ajax({
                url: '/trigger',
                data: JSON.stringify(data),
                contentType: "application/json",
                type: 'POST',
                dataType: 'json'
            })
        })
    }
}