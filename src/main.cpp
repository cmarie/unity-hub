#include <common.h>
#include <device.h>
#include <Arduino.h>

// Needed to read Voltage using ESP.getVcc();
ADC_MODE(ADC_VCC);

// Setup global variables
Config *config = new Config();
ISS_Data *_iss = new ISS_Data();
Weather_Data *_weather = new Weather_Data();
Statistic_Data *_stats = new Statistic_Data();
Sensor_Data *_sensors = new Sensor_Data();
Effects *effect = new Effects();
Device *device = new Device();

// Delete variables to prevent crashes when restarting
void reset()
{
    delete device;
    delete config;
    delete _iss;
    delete _weather;
    delete _stats;
    delete _sensors;
    delete effect;

    ESP.restart();
}

void setup()
{
    device->setup();
}

void loop()
{
    if (config->reset)
    {
        reset();
    }
    else
    {
        device->run();
    }
}
